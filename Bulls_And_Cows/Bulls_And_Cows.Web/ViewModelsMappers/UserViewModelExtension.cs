﻿using Bulls_And_Cows.Services.DtoEntities;
using Bulls_And_Cows.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.ViewModelsMappers
{
    public static class UserViewModelExtension
    {
        public static UserViewModel GetViewModel(this UserDto userDto)
        {
            var userVM = new UserViewModel
            {
                Id = userDto.Id,
                Username = userDto.Username,
                CreatedOn = userDto.CreatedOn,
                NumberOfGames = userDto.NumberOfGames,
                Coefficient = userDto.Coefficient,
                AllScores = userDto.AllScores,
                IsBanned = userDto.IsBanned,
                ReasonForBan = userDto.ReasonForBan
            };

            return userVM;
        }

        public static ICollection<UserViewModel> GetViewModels(this ICollection<UserDto> userDtos)
        {
            return userDtos.Select(GetViewModel).ToList();
        }

        public static UserDto GetDto(this UserViewModel userViewModel)
        {
            var userDto = new UserDto
            {
                Id = userViewModel.Id,
                Username = userViewModel.Username,
                CreatedOn = userViewModel.CreatedOn,
                NumberOfGames = userViewModel.NumberOfGames,
                Coefficient = userViewModel.Coefficient,
                AllScores = userViewModel.AllScores,
                IsBanned = userViewModel.IsBanned,
                ReasonForBan = userViewModel.ReasonForBan
            };

            return userDto;
        }

        public static ICollection<UserDto> GetDtos(this ICollection<UserViewModel> userViewModels)
        {
            return userViewModels.Select(GetDto).ToList();
        }
    }
}
