﻿using Bulls_And_Cows.Services.DtoEntities;
using Bulls_And_Cows.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.ViewModelsMappers
{
    public static class AllGuessesViewModelExtension
    {
        public static AllGuessesViewModel GetViewModel(this AllGuessesDto allGuessesDto)
        {
            var AllGuessesVM = new AllGuessesViewModel
            {
                Guess = allGuessesDto.Guess,
                Bulls = allGuessesDto.Bulls,
                Cows = allGuessesDto.Cows
            };

            return AllGuessesVM;
        }

        public static ICollection<AllGuessesViewModel> GetViewModels(this ICollection<AllGuessesDto> allGuessesDtos)
        {
            return allGuessesDtos.Select(GetViewModel).ToList();
        }

        public static AllGuessesDto GetDto(this AllGuessesViewModel allGuessesViewModel)
        {
            var AllGuessesDto = new AllGuessesDto
            {
                Guess = allGuessesViewModel.Guess,
                Bulls = allGuessesViewModel.Bulls,
                Cows = allGuessesViewModel.Cows
            };

            return AllGuessesDto;
        }

        public static ICollection<AllGuessesDto> GetDtos(this ICollection<AllGuessesViewModel> allGuessesViewModels)
        {
            return allGuessesViewModels.Select(GetDto).ToList();
        }
    }
}
