﻿using Bulls_And_Cows.Services.DtoEntities;
using Bulls_And_Cows.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.ViewModelsMappers
{
    public static class GameViewModelExtension
    {
        public static GameViewModel GetViewModel(this GameDto gameDto)
        {
            var gameVM = new GameViewModel
            {
                Id = gameDto.Id,
                Score = gameDto.Score,
                UserId = gameDto.UserId,
                Bulls = gameDto.Bulls,
                Cows = gameDto.Cows
            };

            gameVM.SpecialNumber.AddRange(gameDto.SpecialNumber);
            gameVM.CurrentGuess.AddRange(gameDto.CurrentGuess);
            gameVM.AllGuesses.AddRange(gameDto.AllGuesses.GetViewModels());

            return gameVM;
        }

        public static ICollection<GameViewModel> GetViewModels(this ICollection<GameDto> gameDtos)
        {
            return gameDtos.Select(GetViewModel).ToList();
        }

        public static GameDto GetDto(this GameViewModel gameViewModel)
        {
            var gameDto = new GameDto
            {
                Id = gameViewModel.Id,
                Score = gameViewModel.Score,
                UserId = gameViewModel.UserId,
                Bulls = gameViewModel.Bulls,
                Cows = gameViewModel.Cows
            };

            gameDto.SpecialNumber.AddRange(gameViewModel.SpecialNumber);
            gameDto.CurrentGuess.AddRange(gameViewModel.CurrentGuess);
            gameDto.AllGuesses.AddRange(gameViewModel.AllGuesses.GetDtos());

            return gameDto;
        }

        public static ICollection<GameDto> GetDtos(this ICollection<GameViewModel> gameViewModels)
        {
            return gameViewModels.Select(GetDto).ToList();
        }
    }
}
