﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bulls_And_Cows.Services.Contracts;
using Bulls_And_Cows.Web.Models;
using Bulls_And_Cows.Web.ViewModelsMappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bulls_And_Cows.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }
        public async Task<IActionResult> GetTop25Users()
        {
            var top25User = await this.userService.GetTop25UsersAsync();
            var top25UserVM = top25User.GetViewModels();

            return View(top25UserVM);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetAllUsers()
        {
            var activeUsers = await this.userService.GetAllUsersAsync("active");
            var bannedUsers = await this.userService.GetAllUsersAsync("banned");
            var activeUsersVM = activeUsers.GetViewModels();
            var bannedUsersVM = bannedUsers.GetViewModels();

            var allUsersVM = new UsersViewModel
            {
                ActiveUsers = activeUsersVM,
                BannedUsers = bannedUsersVM
            };

            var banModalVM = new BanModalViewModel
            {
                usersViewModel = allUsersVM,
            };

            return View(banModalVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBan(BanViewModel banViewModel)
        {
            if (ModelState.IsValid)
            {
                await this.userService.BanUserAsync(banViewModel.UserId, banViewModel.Description);

                return RedirectToAction("GetAllUsers", "User");
            }

            return BadRequest();
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UnbanUser(Guid id)
        {
            try
            {
                await this.userService.UnbanUserAsync(id);
            }
            catch (Exception)
            {

                return BadRequest();
            }

            return RedirectToAction("GetAllUsers", "User");
        }
    }
}
