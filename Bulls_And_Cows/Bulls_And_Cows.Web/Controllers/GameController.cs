﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Contracts;
using Bulls_And_Cows.Web.Models;
using Bulls_And_Cows.Web.ViewModelsMappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Bulls_And_Cows.Web.Controllers
{
    public class GameController : Controller
    {
        private readonly IGameService gameService;
        private readonly UserManager<User> userManager;

        public GameController(IGameService gameService, UserManager<User> userManager)
        {
            this.gameService = gameService;
            this.userManager = userManager;
        }

        //[HttpGet]
        //public IActionResult Create()
        //{
        //    //var gameViewModel = new GameViewModel();
        //    return View();
        //}

        //[HttpPost]
        public async Task<IActionResult> Create()
        {
            var userId = (await userManager.GetUserAsync(User)).Id;

            //var gameDto = gameViewModel.GetDto();

            var gameDto = await this.gameService.CreateGameAsync(userId);
            var gameVM = gameDto.GetViewModel();

            return View("Views/Game/Play.cshtml", gameVM);
        }

       
        [HttpPut]
        public async Task<IActionResult> Play(GameViewModel gameViewModel)
        {
            var gameDto = await this.gameService.PlayAsync(gameViewModel.GetDto());
            var gameVM = gameDto.GetViewModel();

            return View(gameVM);
        }
    }
}
