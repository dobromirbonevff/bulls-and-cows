﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.Models
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public DateTime CreatedOn { get; set; }
        public int NumberOfGames { get; set; }
        public double Coefficient { get; set; }
        public double AllScores { get; set; }
        public bool IsBanned { get; set; }
        [DisplayName("Ban Reason")]
        public string ReasonForBan { get; set; }
    }
}
