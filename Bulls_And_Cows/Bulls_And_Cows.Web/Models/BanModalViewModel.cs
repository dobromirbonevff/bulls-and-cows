﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.Models
{
    public class BanModalViewModel
    {
        public UsersViewModel usersViewModel { get; set; }
        public BanViewModel banViewModel { get; set; }
    }
}
