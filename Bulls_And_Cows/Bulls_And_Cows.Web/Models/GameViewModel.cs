﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.Models
{
    public class GameViewModel
    {
        public GameViewModel()
        {
            this.SpecialNumber = new List<int>();
            this.CurrentGuess = new List<int>();
            this.AllGuesses = new List<AllGuessesViewModel>();
        }
        public Guid Id { get; set; }
        public int Score { get; set; }
        public int Bulls { get; set; }
        public int Cows { get; set; }
        public List<int> SpecialNumber { get; set; } 
        public List<int> CurrentGuess { get; set; }
        public List<AllGuessesViewModel> AllGuesses { get; set; }
        public Guid UserId { get; set; }
    }
}
