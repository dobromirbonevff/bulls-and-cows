﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.Models
{
    public class UsersViewModel
    {
        public ICollection<UserViewModel> ActiveUsers { get; set; }
        public ICollection<UserViewModel> BannedUsers { get; set; }
    }
}
