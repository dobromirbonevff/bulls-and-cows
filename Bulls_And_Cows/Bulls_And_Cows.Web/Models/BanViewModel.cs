﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.Models
{
    public class BanViewModel
    {
        public Guid Id { get; set; }

        [DisplayName("Ban Reason")]
        [Required(ErrorMessage = "You must include a description for the ban")]
        [MinLength(3)]
        [MaxLength(80)]
        public string Description { get; set; }
        public Guid UserId { get; set; }
    }
}
