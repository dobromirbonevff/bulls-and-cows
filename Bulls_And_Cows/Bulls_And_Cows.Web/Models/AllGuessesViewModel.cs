﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Web.Models
{
    public class AllGuessesViewModel
    {
        public string Guess { get; set; }
        public int Bulls { get; set; }
        public int Cows { get; set; }
    }
}
