﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Tests.GameServiceTests
{
    [TestClass]
    public class CreateGameAsync_Should
    {
        [TestMethod]
        public async Task CreateGame_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(CreateGame_When_ParamsAreValid));
            var userId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new GameService(assertContext);
                var result = await sut.CreateGameAsync(user.Id);

                Assert.AreEqual(user.Id, result.UserId);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_When_ParamsAreNotValid()
        {
            var options = Utilities.GetOptions(nameof(CreateGame_When_ParamsAreValid));
            var guid = Guid.NewGuid();

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new GameService(assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateGameAsync(guid));
            }
        }
    }
}
