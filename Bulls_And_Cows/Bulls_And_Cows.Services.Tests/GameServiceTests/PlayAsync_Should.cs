﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.DtoEntities;
using Bulls_And_Cows.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Tests.GameServiceTests
{
    [TestClass]
    public class PlayAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCountOfBullAndCows_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCountOfBullAndCows_When_ParamsAreValid));
            var userId = Guid.NewGuid();
            var gameId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
            };

            var game = new Game
            {
                Id = gameId,
                Score = 1,
                UserId = userId,
            };

            var gameDto = new GameDto
            {
                Id = gameId,
                SpecialNumber = new List<int> { 1, 2, 3, 4 },
                Score = 1,
                UserId = userId,
                CurrentGuess = new List<int> { 1, 3, 5, 6},
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Games.Add(game);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new GameService(assertContext);
                var result = await sut.PlayAsync(gameDto);

                Assert.AreEqual(1, result.Bulls);
                Assert.AreEqual(1, result.Cows);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectNumberOfGames_When_BullsAreEqualsTo4()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectNumberOfGames_When_BullsAreEqualsTo4));
            var userId = Guid.NewGuid();
            var gameId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
            };

            var game = new Game
            {
                Id = gameId,
                Score = 1,
                UserId = userId,
            };

            var gameDto = new GameDto
            {
                Id = gameId,
                SpecialNumber = new List<int> { 1, 2, 3, 4 },
                Score = 1,
                UserId = userId,
                CurrentGuess = new List<int> { 1, 2, 3, 4 },
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Games.Add(game);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new GameService(assertContext);
                var result = await sut.PlayAsync(gameDto);
                var userResult = await assertContext.Users.FirstOrDefaultAsync();

                Assert.AreEqual(1, userResult.NumberOfGames);
            }
        }

    }
}
