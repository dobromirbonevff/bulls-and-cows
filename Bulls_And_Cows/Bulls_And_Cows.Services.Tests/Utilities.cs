﻿using Bulls_And_Cows.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Services.Tests
{
    public static class Utilities
    {
        public static DbContextOptions<Bulls_And_CowsDBContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<Bulls_And_CowsDBContext>()
               .UseInMemoryDatabase(databaseName)
               .Options;
        }
    }
}
