﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetTop25UsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCorrectSequence_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCorrectSequence_When_ParamsAreValid));
            var user1Id = Guid.NewGuid();
            var user2Id = Guid.NewGuid();

            var user1 = new User
            {
                Id = user1Id,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
                Coefficient = 0.4
            };

            var user2 = new User
            {
                Id = user2Id,
                UserName = "Test_User2",
                Email = "Test_User2@bac.com",
                Coefficient = 0.5
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new UserService(assertContext);
                var result = await sut.GetTop25UsersAsync();
                var resultAsList = result.ToList();

                Assert.AreEqual(user2Id, resultAsList[0].Id);
                Assert.AreEqual(user1Id, resultAsList[1].Id);
            }
        }
    }
}
