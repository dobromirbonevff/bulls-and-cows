﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Tests.UserServiceTests
{
    [TestClass]
    public class BanUserAsync_Should
    {
        [TestMethod]
        public async Task BanCorrectUser_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(BanCorrectUser_When_ParamsAreValid));
            var userId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new UserService(assertContext);
                var result = await sut.BanUserAsync(userId, "Ban");
                var userResult = await assertContext.Users.FirstOrDefaultAsync();

                Assert.IsTrue(userResult.IsBanned);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreNotValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_ParamsAreNotValid));
            var guid = Guid.NewGuid();

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new UserService(assertContext);
                var result = await sut.BanUserAsync(guid, "Ban");

                Assert.IsFalse(result);
            }
        }
    }
}
