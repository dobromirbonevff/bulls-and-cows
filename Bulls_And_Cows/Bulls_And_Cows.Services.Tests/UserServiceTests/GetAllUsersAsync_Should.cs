﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Tests.UserServiceTests
{
    [TestClass]
    public class GetAllUsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCountOfUsers_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectCountOfUsers_When_ParamsAreValid));
            var user1Id = Guid.NewGuid();
            var user2Id = Guid.NewGuid();

            var user1 = new User
            {
                Id = user1Id,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
            };

            var user2 = new User
            {
                Id = user2Id,
                UserName = "Test_User2",
                Email = "Test_User2@bac.com",
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user1);
                arrangeContext.Users.Add(user2);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new UserService(assertContext);
                var result = await sut.GetAllUsersAsync("active");

                Assert.AreEqual(2, result.Count);
            }
        }


    }
}
