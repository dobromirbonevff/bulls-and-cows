﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Tests.UserServiceTests
{
    [TestClass]
    public class UnbanUserAsync_Should
    {
        [TestMethod]
        public async Task UnbanCorrectUser_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(UnbanCorrectUser_When_ParamsAreValid));
            var userId = Guid.NewGuid();
            var banId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@bac.com",
                IsBanned = true
            };

            var ban = new Ban
            {
                Id = banId,
                User = user,
                Description = "Ban"
            };

            using (var arrangeContext = new Bulls_And_CowsDBContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Bans.Add(ban);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new UserService(assertContext);
                var result = await sut.UnbanUserAsync(userId);
                var userResult = await assertContext.Users.FirstOrDefaultAsync();

                Assert.IsFalse(userResult.IsBanned);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_UserIdIsNotValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnFalse_When_UserIdIsNotValid));
            var guid = Guid.NewGuid();

            //Act & Assert
            using (var assertContext = new Bulls_And_CowsDBContext(options))
            {
                var sut = new UserService(assertContext);
                var result = await sut.UnbanUserAsync(guid);

                Assert.IsFalse(result);
            }
        }
    }
}
