﻿using Bulls_And_Cows.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Data.Configurations
{
    public class BanConfiguration : IEntityTypeConfiguration<Ban>
    {
        public void Configure(EntityTypeBuilder<Ban> builder)
        {
            builder
                .HasKey(b => b.Id);

            builder
                .Property(b => b.Description)
                .IsRequired();

            builder.HasOne(b => b.User)
                .WithMany(u => u.Bans);
        }
    }
}
