﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bulls_And_Cows.Data.Migrations
{
    public partial class UpdateSeeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("1579e316-e392-4aca-8c33-1deb8e9a6f24"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("182d893e-a42d-4a7f-badc-113e3ad18c01"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("1f0131e5-e78c-4141-864e-93035642ff5f"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("23888666-d4e9-4b92-a91b-4c2884195ac4"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("29e2e108-a247-458d-b033-9dd2378dd046"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("3bad67be-43d8-4cab-ac82-ea945909d9f3"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("3e8ec22a-94ac-4cb8-9b51-ace9f814bc15"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("424d61e9-eba8-4320-be42-d8afaa386f8a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("44ee5ebb-f501-4943-b90b-cb8e30f42d26"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("473b5de8-a76a-435c-ab76-2ffb93e0e057"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("4d1e4c39-45a7-412b-be48-e938e2f30486"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("4e6f7834-5f92-4798-9a06-0e1bb1ed400f"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("5048f1b8-8626-4379-834e-d279a71f17fc"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("5ba4a3ca-8a64-49ca-8270-559b2665851e"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("5bc2e41e-9641-4876-9698-fcb4f3d98ac9"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("7d23eb4f-4b7f-4c1c-baad-b40230d17d50"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("895c2527-b642-4ce4-ba75-2907ba748174"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("98e76320-a072-404d-a76a-9505381c4e2e"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("99d95930-394e-4484-a47c-270cdbdfa2fa"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("a4cda3b4-9a23-45fc-b278-38a741e2186d"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b4a0830b-6366-468e-869d-0b5cd8520b64"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b512be7e-45de-4e84-a42f-1b6237b708df"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b7bdcc28-348d-4f29-b68f-6ffd0fe04a46"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("bfc304d7-4105-4cd8-b0c4-c29a24f0b993"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("daaf2544-63b6-4378-b7c8-bf1c4ffe87e7"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("dab4f704-15ae-41af-9d87-cc5243c6ff70"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("43a08cc4-76ae-46d8-9e2c-cde7b0479146"),
                column: "ConcurrencyStamp",
                value: "c4f3c64c-6487-4b22-b6b2-6254d1988a1a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("cdde12b9-e61a-4748-a239-c7331b4fb6a8"),
                column: "ConcurrencyStamp",
                value: "000d91ee-59c6-4bc0-bcdf-78a84194c220");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("aea4f481-df4b-4272-9d12-022293d98e48"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "207ec178-c0f6-46be-ad01-1f6d0100510e", new DateTime(2020, 9, 16, 18, 37, 20, 276, DateTimeKind.Utc).AddTicks(7189), "AQAAAAEAACcQAAAAEKZ218KghGN3yQ5/u5d8+rzWEHAzSL0ItPMDaTY5p1A4Dhe5g+omnup0/JSkdt9M0g==" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AllScores", "Coefficient", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "IsBanned", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "NumberOfGames", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("e9ec9baf-17e7-441c-8e97-ecfd71ffc178"), 0, 0.0, 0.11, "76b37d44-4ebb-475b-9632-a603fe6475df", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8602), "Bombur@bac.com", false, false, false, null, null, null, 7, null, null, false, null, false, "Bombur@bac.com" },
                    { new Guid("88e4c396-8353-4a77-8ede-6667f4df39d2"), 0, 0.0, 0.33300000000000002, "c292b0f7-3e97-4974-9037-d2f0d45bce84", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8590), "Bofur@bac.com", false, false, false, null, null, null, 11, null, null, false, null, false, "Bofur@bac.com" },
                    { new Guid("b15c2eaa-7538-4ddc-9759-285cc3fbd65b"), 0, 0.0, 0.17999999999999999, "ac54b704-32a5-4436-80ea-c8d0ed2c380a", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8582), "Nori@bac.com", false, false, false, null, null, null, 15, null, null, false, null, false, "Nori@bac.com" },
                    { new Guid("54a5ba7f-25b7-473f-8d80-cea5a88ff94a"), 0, 0.0, 0.13, "c1319bc0-be46-4396-87c4-7e203f9b68a6", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8574), "Dwalin@bac.com", false, false, false, null, null, null, 17, null, null, false, null, false, "Dwalin@bac.com" },
                    { new Guid("0b945809-c437-48f5-81a3-3558d47898eb"), 0, 0.0, 0.14999999999999999, "338f25eb-5231-41c0-bb6c-399c9cd06fd4", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8566), "Balin@bac.com", false, false, false, null, null, null, 21, null, null, false, null, false, "Balin@bac.com" },
                    { new Guid("d19a7eec-e823-4fb9-a52f-9ab629f30305"), 0, 0.0, 0.19, "9e94ca79-4552-4688-aad2-d8c6a7da8f0a", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8554), "Kili@bac.com", false, false, false, null, null, null, 23, null, null, false, null, false, "Kili@bac.com" },
                    { new Guid("d082f1fb-26ec-4d59-90a4-6575165ec4fd"), 0, 0.0, 0.27000000000000002, "c672449e-5594-4137-be95-3b0dd4c41f61", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8546), "Fili@bac.com", false, false, false, null, null, null, 9, null, null, false, null, false, "Fili@bac.com" },
                    { new Guid("27c1415b-3131-4360-85dd-1697219bded6"), 0, 0.0, 0.28999999999999998, "f3369ddd-82fc-4b31-bf24-f466d1b24bdb", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8538), "ThorinOakenshield@bac.com", false, false, false, null, null, null, 7, null, null, false, null, false, "ThorinOakenshield@bac.com" },
                    { new Guid("dacddfeb-76c1-4476-b5f7-28b8a07ef039"), 0, 0.0, 0.25, "146aa53e-3e77-4060-8ffe-ed3575d644d0", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8530), "Treebeard@bac.com", false, false, false, null, null, null, 46, null, null, false, null, false, "Treebeard@bac.com" },
                    { new Guid("066662fe-dce2-4c05-80cc-bb31a7b2d872"), 0, 0.0, 0.20000000000000001, "5cb3b5d9-eab9-4b5e-9e95-2317f6a0fb8f", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8518), "PippinTookn@bac.com", false, false, false, null, null, null, 9, null, null, false, null, false, "PippinTookn@bac.com" },
                    { new Guid("a2729982-4d36-4e86-8b24-cff1430d1ccf"), 0, 0.0, 0.40000000000000002, "84bddc3a-05c7-4de1-b9b3-00899fbc19af", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8509), "Sauron@bac.com", false, false, false, null, null, null, 6, null, null, false, null, false, "Sauron@bac.com" },
                    { new Guid("caf0848c-8950-45eb-b25a-129b38938d67"), 0, 0.0, 0.42999999999999999, "05e67c7b-6b0f-472e-bc2c-4656ca8a0c0d", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8501), "Saruman@bac.com", false, false, false, null, null, null, 34, null, null, false, null, false, "Saruman@bac.com" },
                    { new Guid("d1877ab4-609d-4eda-a6c2-bd2effabd7f2"), 0, 0.0, 0.45000000000000001, "689e8664-bbf3-4fee-8521-2b78f964707a", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8481), "Legolas@bac.com", false, false, false, null, null, null, 5, null, null, false, null, false, "Legolas@bac.com" },
                    { new Guid("5d592b05-7234-4945-9950-85588bf2b541"), 0, 0.0, 0.47999999999999998, "bc88c861-0a5d-410e-abdd-77c555043f04", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8473), "Isildur@bac.com", false, false, false, null, null, null, 3, null, null, false, null, false, "Isildur@bac.com" },
                    { new Guid("f78b4f77-e019-4659-93ba-81c118e20a61"), 0, 0.0, 0.29999999999999999, "d5d31b4f-5d8b-4eec-9472-c3f416999152", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8465), "Gollum@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Gollum@bac.com" },
                    { new Guid("409e29b1-54c0-4333-bf67-2e4e5b4ecc64"), 0, 0.0, 0.34999999999999998, "a79f2477-da90-4d86-b60a-65bb620b611a", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8455), "Gimli@bac.com", false, false, false, null, null, null, 14, null, null, false, null, false, "Gimli@bac.com" },
                    { new Guid("8d453a63-5b69-4b13-8f3a-8fd3cac2b8a4"), 0, 0.0, 0.32000000000000001, "5ff4632d-ede8-408e-8d3d-e2aada079773", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8440), "Gandalf@bac.com", false, false, false, null, null, null, 23, null, null, false, null, false, "Gandalf@bac.com" },
                    { new Guid("60878dba-ebed-4cde-869a-c07200ce60a3"), 0, 0.0, 0.55000000000000004, "c5acac61-9202-4136-b15a-40d629ae60fd", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8230), "Galadriel@bac.com", false, false, false, null, null, null, 7, null, null, false, null, false, "Galadriel@bac.com" },
                    { new Guid("d85dd96c-0b83-47b1-82c6-15517eeb6ee4"), 0, 0.0, 0.75, "e3888d84-1f9b-4192-9964-b5a7dee88da9", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8216), "Faramir@bac.com", false, false, false, null, null, null, 22, null, null, false, null, false, "Faramir@bac.com" },
                    { new Guid("bf4649ab-1eb5-4e95-be09-0b2577b2413d"), 0, 0.0, 0.67000000000000004, "0b183f49-34c3-4192-b107-c53ada629601", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8201), "Elrond@bac.com", false, false, false, null, null, null, 19, null, null, false, null, false, "Elrond@bac.com" },
                    { new Guid("474478e9-9625-4d23-a421-823eafede84a"), 0, 0.0, 0.60999999999999999, "ab8e3ec8-5fca-432d-843a-2eebcf0b6df3", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8178), "Boromir@bac.com", false, false, false, null, null, null, 16, null, null, false, null, false, "Boromir@bac.com" },
                    { new Guid("db58e49e-b6db-4029-a795-c9f9f68b25a2"), 0, 0.0, 0.68000000000000005, "1b7b6819-2585-4304-bb49-2f312d4df328", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8169), "FrodoBaggins@bac.com", false, false, false, null, null, null, 11, null, null, false, null, false, "FrodoBaggins@bac.com" },
                    { new Guid("6ae06c33-d712-4d0b-b6f0-2740c0f49d71"), 0, 0.0, 0.53000000000000003, "e9757fa7-beaf-4efa-8c0f-f8c0f14b1c91", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8151), "BilboBaggins@bac.com", false, false, false, null, null, null, 7, null, null, false, null, false, "BilboBaggins@bac.com" },
                    { new Guid("d69c8ab0-dba4-4ed7-a203-b6df9f229c16"), 0, 0.0, 0.51000000000000001, "11dce45b-ab41-4c24-a059-fdddf201cbfd", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(7985), "Arwen@bac.com", false, false, false, null, null, null, 9, null, null, false, null, false, "Arwen@bac.com" },
                    { new Guid("3ae3dcf3-f5ff-458b-a9e5-d6cf393d5db0"), 0, 0.0, 0.48999999999999999, "77ae879c-28d9-4da3-813d-b58d6ae5e69f", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(8493), "Radagast@bac.com", false, false, false, null, null, null, 33, null, null, false, null, false, "Radagast@bac.com" },
                    { new Guid("16d6003c-ee13-4c41-b059-288d996714da"), 0, 0.0, 0.56000000000000005, "d3aef481-f380-4394-82b8-d62bc8f998ef", new DateTime(2020, 9, 16, 18, 37, 20, 311, DateTimeKind.Utc).AddTicks(5477), "Aragorn@bac.com", false, false, false, null, null, null, 6, null, null, false, null, false, "Aragorn@bac.com" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("066662fe-dce2-4c05-80cc-bb31a7b2d872"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("0b945809-c437-48f5-81a3-3558d47898eb"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("16d6003c-ee13-4c41-b059-288d996714da"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("27c1415b-3131-4360-85dd-1697219bded6"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("3ae3dcf3-f5ff-458b-a9e5-d6cf393d5db0"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("409e29b1-54c0-4333-bf67-2e4e5b4ecc64"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("474478e9-9625-4d23-a421-823eafede84a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("54a5ba7f-25b7-473f-8d80-cea5a88ff94a"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("5d592b05-7234-4945-9950-85588bf2b541"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("60878dba-ebed-4cde-869a-c07200ce60a3"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("6ae06c33-d712-4d0b-b6f0-2740c0f49d71"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("88e4c396-8353-4a77-8ede-6667f4df39d2"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("8d453a63-5b69-4b13-8f3a-8fd3cac2b8a4"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("a2729982-4d36-4e86-8b24-cff1430d1ccf"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("b15c2eaa-7538-4ddc-9759-285cc3fbd65b"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("bf4649ab-1eb5-4e95-be09-0b2577b2413d"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("caf0848c-8950-45eb-b25a-129b38938d67"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d082f1fb-26ec-4d59-90a4-6575165ec4fd"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d1877ab4-609d-4eda-a6c2-bd2effabd7f2"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d19a7eec-e823-4fb9-a52f-9ab629f30305"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d69c8ab0-dba4-4ed7-a203-b6df9f229c16"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d85dd96c-0b83-47b1-82c6-15517eeb6ee4"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("dacddfeb-76c1-4476-b5f7-28b8a07ef039"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("db58e49e-b6db-4029-a795-c9f9f68b25a2"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("e9ec9baf-17e7-441c-8e97-ecfd71ffc178"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("f78b4f77-e019-4659-93ba-81c118e20a61"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("43a08cc4-76ae-46d8-9e2c-cde7b0479146"),
                column: "ConcurrencyStamp",
                value: "6206b8ce-53c4-44fa-a693-32371fb01807");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("cdde12b9-e61a-4748-a239-c7331b4fb6a8"),
                column: "ConcurrencyStamp",
                value: "5f4ecd1e-ad02-4f70-9470-7c72382f6f3f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("aea4f481-df4b-4272-9d12-022293d98e48"),
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "0b70fb92-9205-47a0-bfc5-2180e5550487", new DateTime(2020, 9, 16, 18, 24, 12, 252, DateTimeKind.Utc).AddTicks(4761), "AQAAAAEAACcQAAAAELOo9Rc2SqtUnhQozTw2wzHn8MDiIn8y3VJqLx2imQpn2nuWxLsrX+KGnyNavRpGGA==" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AllScores", "Coefficient", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "IsBanned", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "NumberOfGames", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("7d23eb4f-4b7f-4c1c-baad-b40230d17d50"), 0, 0.0, 0.11, "da3de651-e369-4bbf-8383-1dd4a9b0abb7", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9446), "Bombur@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Bombur@bac.com" },
                    { new Guid("5ba4a3ca-8a64-49ca-8270-559b2665851e"), 0, 0.0, 0.33300000000000002, "902d4444-dbce-4728-8fa4-1092e6b2e4ee", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9429), "Bofur@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Bofur@bac.com" },
                    { new Guid("4d1e4c39-45a7-412b-be48-e938e2f30486"), 0, 0.0, 0.17999999999999999, "890b339a-09da-4f20-8e56-e9e84b6a3e7c", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9411), "Nori@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Nori@bac.com" },
                    { new Guid("182d893e-a42d-4a7f-badc-113e3ad18c01"), 0, 0.0, 0.13, "c2a9c9fa-1362-47ac-b018-7b870dac0e46", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9293), "Dwalin@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Dwalin@bac.com" },
                    { new Guid("98e76320-a072-404d-a76a-9505381c4e2e"), 0, 0.0, 0.14999999999999999, "e22f805f-e47c-4e2b-a024-f2e129378dcb", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9276), "Balin@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Balin@bac.com" },
                    { new Guid("3e8ec22a-94ac-4cb8-9b51-ace9f814bc15"), 0, 0.0, 0.19, "97110847-752a-4bf3-a889-eedf0331484b", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9261), "Kili@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Kili@bac.com" },
                    { new Guid("473b5de8-a76a-435c-ab76-2ffb93e0e057"), 0, 0.0, 0.27000000000000002, "869dc2a8-a33c-4f7e-a274-ad97e5fb46b2", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9244), "Fili@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Fili@bac.com" },
                    { new Guid("5bc2e41e-9641-4876-9698-fcb4f3d98ac9"), 0, 0.0, 0.28999999999999998, "46c3fd13-6dd7-4283-9d0b-6d77a9bd2f08", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9223), "ThorinOakenshield@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "ThorinOakenshield@bac.com" },
                    { new Guid("a4cda3b4-9a23-45fc-b278-38a741e2186d"), 0, 0.0, 0.25, "684e3578-8983-4a8d-9945-07ef7eee6548", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9203), "Treebeard@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Treebeard@bac.com" },
                    { new Guid("5048f1b8-8626-4379-834e-d279a71f17fc"), 0, 0.0, 0.20000000000000001, "24712601-18ee-4d8c-a2f6-5489f85c0b96", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9194), "PippinTookn@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "PippinTookn@bac.com" },
                    { new Guid("b512be7e-45de-4e84-a42f-1b6237b708df"), 0, 0.0, 0.40000000000000002, "382da8d5-a049-47ef-9e4e-0b06b5c05100", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9185), "Sauron@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Sauron@bac.com" },
                    { new Guid("44ee5ebb-f501-4943-b90b-cb8e30f42d26"), 0, 0.0, 0.42999999999999999, "60057c96-7ae1-472e-8234-cc90224d2320", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9172), "Saruman@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Saruman@bac.com" },
                    { new Guid("895c2527-b642-4ce4-ba75-2907ba748174"), 0, 0.0, 0.45000000000000001, "1a8373dc-779b-4e5f-97ea-d0c78cc2e9b0", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9153), "Legolas@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Legolas@bac.com" },
                    { new Guid("1f0131e5-e78c-4141-864e-93035642ff5f"), 0, 0.0, 0.47999999999999998, "a0b87cce-7fcb-4c0c-a8ff-ac3ac681b077", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9145), "Isildur@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Isildur@bac.com" },
                    { new Guid("23888666-d4e9-4b92-a91b-4c2884195ac4"), 0, 0.0, 0.29999999999999999, "60bb1807-3da6-409f-bd58-4ec4309a3606", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9131), "Gollum@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Gollum@bac.com" },
                    { new Guid("dab4f704-15ae-41af-9d87-cc5243c6ff70"), 0, 0.0, 0.34999999999999998, "b8d827bd-020c-4480-94fb-5492f091e4dc", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9114), "Gimli@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Gimli@bac.com" },
                    { new Guid("daaf2544-63b6-4378-b7c8-bf1c4ffe87e7"), 0, 0.0, 0.32000000000000001, "5cfa7dcb-5ddc-4d44-9e52-b0da7d88abcf", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9097), "Gandalf@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Gandalf@bac.com" },
                    { new Guid("b7bdcc28-348d-4f29-b68f-6ffd0fe04a46"), 0, 0.0, 0.55000000000000004, "21676e4c-b4b0-40a6-89cd-45b2c295fe64", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9081), "Galadriel@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Galadriel@bac.com" },
                    { new Guid("bfc304d7-4105-4cd8-b0c4-c29a24f0b993"), 0, 0.0, 0.75, "65e98233-be7c-4f56-9eb2-fa0f54d5fe57", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9059), "Faramir@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Faramir@bac.com" },
                    { new Guid("4e6f7834-5f92-4798-9a06-0e1bb1ed400f"), 0, 0.0, 0.67000000000000004, "f3757b4e-eefc-44d2-bfbe-cbd0cf570d0e", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9043), "Elrond@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Elrond@bac.com" },
                    { new Guid("29e2e108-a247-458d-b033-9dd2378dd046"), 0, 0.0, 0.60999999999999999, "15ca17d8-35cf-49ba-b165-93618b13bbb1", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9026), "Boromir@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Boromir@bac.com" },
                    { new Guid("1579e316-e392-4aca-8c33-1deb8e9a6f24"), 0, 0.0, 0.68000000000000005, "964c8b52-c2bd-437c-acff-0553f15a1474", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9015), "FrodoBaggins@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "FrodoBaggins@bac.com" },
                    { new Guid("424d61e9-eba8-4320-be42-d8afaa386f8a"), 0, 0.0, 0.53000000000000003, "c5e90fad-a156-491e-a492-e505cc23347e", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(8988), "BilboBaggins@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "BilboBaggins@bac.com" },
                    { new Guid("3bad67be-43d8-4cab-ac82-ea945909d9f3"), 0, 0.0, 0.51000000000000001, "fe6ba957-2f2d-4597-9916-1f09fb0e626f", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(8924), "Arwen@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Arwen@bac.com" },
                    { new Guid("b4a0830b-6366-468e-869d-0b5cd8520b64"), 0, 0.0, 0.48999999999999999, "b0a1fdcc-b3a6-4f56-a438-8c1126f3701f", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(9163), "Radagast@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Radagast@bac.com" },
                    { new Guid("99d95930-394e-4484-a47c-270cdbdfa2fa"), 0, 0.0, 0.56000000000000005, "9ce43846-8871-4926-a525-b62c0cb0fd31", new DateTime(2020, 9, 16, 18, 24, 12, 286, DateTimeKind.Utc).AddTicks(7444), "Aragorn@bac.com", false, false, false, null, null, null, 0, null, null, false, null, false, "Aragorn@bac.com" }
                });
        }
    }
}
