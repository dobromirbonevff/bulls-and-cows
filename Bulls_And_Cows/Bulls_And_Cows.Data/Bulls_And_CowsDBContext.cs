﻿using Bulls_And_Cows.Data.Configurations;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Data.Seeder;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Data
{
    public class Bulls_And_CowsDBContext : IdentityDbContext<User, Role, Guid>
    {
        public Bulls_And_CowsDBContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Ban> Bans { get; set; }
        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BanConfiguration());
            builder.ApplyConfiguration(new GameConfiguration());
            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());

            builder.Seeder();

            base.OnModelCreating(builder);
        }
    }
}
