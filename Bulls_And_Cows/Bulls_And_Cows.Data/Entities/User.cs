﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Data.Entities
{
    public class User : IdentityUser<Guid>
    {
        public User()
        {
            this.Bans = new List<Ban>();
            this.Games = new List<Game>();
        }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public int NumberOfGames { get; set; }
        public double Coefficient { get; set; }
        public double AllScores { get; set; }
        public bool IsBanned { get; set; }
        public ICollection<Ban> Bans { get; set; }
        public ICollection<Game> Games { get; set; }
    }
}
