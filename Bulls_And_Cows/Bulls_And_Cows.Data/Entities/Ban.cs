﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bulls_And_Cows.Data.Entities
{
    public class Ban
    {
        public Guid Id { get; set; }

        [MinLength(10), MaxLength(200)]
        public string Description { get; set; }
        public User User { get; set; }
    }
}
