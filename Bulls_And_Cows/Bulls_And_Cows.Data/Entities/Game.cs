﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Data.Entities
{
    public class Game
    {
        public Guid Id { get; set; }
        public int Score { get; set; }
        public User User { get; set; }
        public Guid UserId { get; set; }
    }
}
