﻿using Bulls_And_Cows.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void Seeder(this ModelBuilder builder)
        {
            //Role
            builder.Entity<Role>().HasData(
                new Role { Id = Guid.Parse("CDDE12B9-E61A-4748-A239-C7331B4FB6A8"), Name = "Admin", NormalizedName = "ADMIN" },
                new Role { Id = Guid.Parse("43A08CC4-76AE-46D8-9E2C-CDE7B0479146"), Name = "Member", NormalizedName = "MEMBER" }
            );

            //Admin Account
            var hasher = new PasswordHasher<User>();

            User adminUser = new User
            {
                Id = Guid.Parse("AEA4F481-DF4B-4272-9D12-022293D98E48"),
                UserName = "admin@bac.com",
                NormalizedUserName = "ADMIN@BAC.COM",
                Email = "admin@bac.com",
                NormalizedEmail = "ADMIN@BAC.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "DC6E275DD1E24957A7781D42BB68293B"
            };

            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin1");

            builder.Entity<User>().HasData(adminUser);

            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>
                {
                    RoleId = Guid.Parse("CDDE12B9-E61A-4748-A239-C7331B4FB6A8"),
                    UserId = adminUser.Id
                });

            builder.Entity<User>().HasData(
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Aragorn@bac.com",
                Email = "Aragorn@bac.com",
                Coefficient = 0.56,
                NumberOfGames = 6
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Arwen@bac.com",
                Email = "Arwen@bac.com",
                Coefficient = 0.51,
                NumberOfGames = 9
            },
             new User
            {
                Id = Guid.NewGuid(),
                UserName = "BilboBaggins@bac.com",
                Email = "BilboBaggins@bac.com",
                Coefficient = 0.53,
                NumberOfGames = 7
             },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "FrodoBaggins@bac.com",
                Email = "FrodoBaggins@bac.com",
                Coefficient = 0.68,
                NumberOfGames = 11
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Boromir@bac.com",
                Email = "Boromir@bac.com",
                Coefficient = 0.61,
                NumberOfGames = 16
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Elrond@bac.com",
                Email = "Elrond@bac.com",
                Coefficient = 0.67,
                NumberOfGames = 19
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Faramir@bac.com",
                Email = "Faramir@bac.com",
                Coefficient = 0.75,
                NumberOfGames = 22
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Galadriel@bac.com",
                Email = "Galadriel@bac.com",
                Coefficient = 0.55,
                NumberOfGames = 7
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Gandalf@bac.com",
                Email = "Gandalf@bac.com",
                Coefficient = 0.32,
                NumberOfGames = 23
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Gimli@bac.com",
                Email = "Gimli@bac.com",
                Coefficient = 0.35,
                NumberOfGames = 14
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Gollum@bac.com",
                Email = "Gollum@bac.com",
                Coefficient = 0.30
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Isildur@bac.com",
                Email = "Isildur@bac.com",
                Coefficient = 0.48,
                NumberOfGames = 3
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Legolas@bac.com",
                Email = "Legolas@bac.com",
                Coefficient = 0.45,
                NumberOfGames = 5
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Radagast@bac.com",
                Email = "Radagast@bac.com",
                Coefficient = 0.49,
                NumberOfGames = 33
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Saruman@bac.com",
                Email = "Saruman@bac.com",
                Coefficient = 0.43,
                NumberOfGames = 34
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Sauron@bac.com",
                Email = "Sauron@bac.com",
                Coefficient = 0.40,
                NumberOfGames = 6
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "PippinTookn@bac.com",
                Email = "PippinTookn@bac.com",
                Coefficient = 0.20,
                NumberOfGames = 9
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Treebeard@bac.com",
                Email = "Treebeard@bac.com",
                Coefficient = 0.25,
                NumberOfGames = 46
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "ThorinOakenshield@bac.com",
                Email = "ThorinOakenshield@bac.com",
                Coefficient = 0.29,
                NumberOfGames = 7
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Fili@bac.com",
                Email = "Fili@bac.com",
                Coefficient = 0.27,
                NumberOfGames = 9
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Kili@bac.com",
                Email = "Kili@bac.com",
                Coefficient = 0.19,
                NumberOfGames = 23
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Balin@bac.com",
                Email = "Balin@bac.com",
                Coefficient = 0.15,
                NumberOfGames = 21
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Dwalin@bac.com",
                Email = "Dwalin@bac.com",
                Coefficient = 0.13,
                NumberOfGames = 17
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Nori@bac.com",
                Email = "Nori@bac.com",
                Coefficient = 0.18,
                NumberOfGames = 15
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Bofur@bac.com",
                Email = "Bofur@bac.com",
                Coefficient = 0.333,
                NumberOfGames = 11
            },
            new User
            {
                Id = Guid.NewGuid(),
                UserName = "Bombur@bac.com",
                Email = "Bombur@bac.com",
                Coefficient = 0.11,
                NumberOfGames = 7
            });
        }
    }


}
