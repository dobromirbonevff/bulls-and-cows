﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Contracts;
using Bulls_And_Cows.Services.DtoEntities;
using Bulls_And_Cows.Services.DtoMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Services
{
    public class GameService : IGameService
    {
        private readonly Bulls_And_CowsDBContext context;
        public GameService(Bulls_And_CowsDBContext context)
        {
            this.context = context;
        }
        
        public async Task<GameDto> CreateGameAsync(Guid userId)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var newGame = new Game
            {
                UserId = userId
            };

            await this.context.Games.AddAsync(newGame);
            await this.context.SaveChangesAsync();

            var gameDto = newGame.GetDto();

            var specialNumber = CreateSpecialNumber();

            gameDto.SpecialNumber.AddRange(specialNumber);

            return gameDto;
        }

        public List<int> CreateSpecialNumber()
        {
            var rnd = new Random();
            var specialNumber = Enumerable.Range(1, 9).OrderBy(x => rnd.Next()).Take(4).ToList();

            return specialNumber;
        }

        public async Task<GameDto> PlayAsync(GameDto gameDto)
        {
            gameDto.Bulls = 0;
            gameDto.Cows = 0;

            for (int i = 0; i < 4; i++)
            {
                if (gameDto.CurrentGuess[i] == gameDto.SpecialNumber[i])
                {
                    gameDto.Bulls++;
                }
                else if (gameDto.SpecialNumber.Contains(gameDto.CurrentGuess[i]))
                {
                    gameDto.Cows++;
                }
            }
            
            var currentGuessAsString = string.Join(' ', gameDto.CurrentGuess);
            
            var currentGuess = new AllGuessesDto
            {
                Guess = currentGuessAsString,
                Bulls = gameDto.Bulls,
                Cows = gameDto.Cows
            };

            gameDto.AllGuesses.Add(currentGuess);

            gameDto.Score++;

            if (gameDto.Bulls == 4)
            {
                var user = await context.Users
                .Where(u => u.Id == gameDto.UserId)
                .FirstOrDefaultAsync();

                user.NumberOfGames++;
                user.AllScores += gameDto.Score;
                var coefficient = user.NumberOfGames / user.AllScores;
                user.Coefficient = coefficient;

                this.context.Users.Update(user);
            }

            var currentGame = gameDto.GetEntity();

            this.context.Games.Update(currentGame);
            await this.context.SaveChangesAsync();

            return gameDto;
        }
    }
}
