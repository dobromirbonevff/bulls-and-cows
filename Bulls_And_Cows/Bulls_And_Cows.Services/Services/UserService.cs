﻿using Bulls_And_Cows.Data;
using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.Contracts;
using Bulls_And_Cows.Services.DtoEntities;
using Bulls_And_Cows.Services.DtoMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Services
{
    public class UserService : IUserService
    {
        private readonly Bulls_And_CowsDBContext context;
        public UserService(Bulls_And_CowsDBContext context)
        {
            this.context = context;
        }

        public async Task<ICollection<UserDto>> GetAllUsersAsync(string param)
        {
            var users = new List<User>();

            if (param == "active")
            {
                users = await this.context.Users
                    .Where(u => u.IsBanned == false
                    && u.UserName != "admin@bac.com")
                    .ToListAsync();
            }
            else if (param == "banned")
            {
                users = await this.context.Users
                    .Include(b => b.Bans)
                    .Where(u => u.IsBanned == true)
                    .ToListAsync();
            }

            var usersDto = users.GetDtos();

            return usersDto;
        }

        public async Task<ICollection<UserDto>> GetTop25UsersAsync()
        {
            var top25Users = await this.context.Users
                    .Where(u => u.IsBanned == false
                    && u.UserName != "admin@bac.com")
                    .OrderByDescending(u => u.Coefficient)
                    .Take(25)
                    .ToListAsync();

            var usersDtos = top25Users.GetDtos();

            return usersDtos;
        }

        public async Task<bool> BanUserAsync(Guid id, string description)
        {
            try
            {
                var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.Id == id);

                if (user == null)
                {
                    throw new ArgumentNullException();
                }

                var ban = new Ban
                {
                    Description = description,
                    User = user,
                };

                user.Bans.Add(ban);
                user.IsBanned = true;
                user.LockoutEnabled = true;
                user.LockoutEnd = DateTime.UtcNow.AddDays(365);
                await this.context.Bans.AddAsync(ban);
                await this.context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        public async Task<bool> UnbanUserAsync(Guid id)
        {
            try
            {
                var ban = await this.context.Bans
                     .Include(u => u.User)
                     .FirstOrDefaultAsync(b => b.User.Id == id);

                if (ban == null)
                {
                    throw new ArgumentNullException();
                }

                ban.User.IsBanned = false;
                ban.User.LockoutEnd = DateTime.UtcNow;
                ban.User.LockoutEnabled = false;

                await this.context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }
    }
}
