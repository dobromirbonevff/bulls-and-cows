﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Services.DtoEntities
{
    public class GameDto
    {
        public GameDto()
        {
            this.SpecialNumber = new List<int>();
            this.CurrentGuess = new List<int>();
            this.AllGuesses = new List<AllGuessesDto>();
        }
        public Guid Id { get; set; }
        public int Score { get; set; }
        public int Bulls { get; set; }
        public int Cows { get; set; }
        public List<int> SpecialNumber { get; set; }
        public List<int> CurrentGuess { get; set; }
        public List<AllGuessesDto> AllGuesses { get; set; }
        public Guid UserId { get; set; }
    }
}
