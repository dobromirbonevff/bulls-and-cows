﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Services.DtoEntities
{
    public class AllGuessesDto
    {
        public string Guess { get; set; }
        public int Bulls { get; set; }
        public int Cows { get; set; }
    }
}
