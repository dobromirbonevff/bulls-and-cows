﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bulls_And_Cows.Services.DtoEntities
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public DateTime CreatedOn { get; set; } 
        public int NumberOfGames { get; set; }
        public double Coefficient { get; set; }
        public double AllScores { get; set; }
        public bool IsBanned { get; set; }
        public string ReasonForBan { get; set; }
    }
}
