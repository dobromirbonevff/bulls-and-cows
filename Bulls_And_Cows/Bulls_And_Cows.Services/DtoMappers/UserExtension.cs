﻿using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.DtoEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bulls_And_Cows.Services.DtoMappers
{
    public static class UserExtension
    {
        public static UserDto GetDto(this User user)
        {
            var userDto = new UserDto
            {
                Id = user.Id,
                Username = user.UserName,
                CreatedOn = user.CreatedOn,
                NumberOfGames = user.NumberOfGames,
                Coefficient = user.Coefficient,
                AllScores = user.AllScores,
                IsBanned = user.IsBanned,
                ReasonForBan = user.Bans
                      .Select(b => b.Description)
                      .FirstOrDefault()
            };

            return userDto;
        }

        public static ICollection<UserDto> GetDtos(this ICollection<User> users)
        {
            return users.Select(GetDto).ToList();
        }

        public static User GetEntity(this UserDto userDto)
        {
            var user = new User
            {
                Id = userDto.Id,
                UserName = userDto.Username,
                CreatedOn = userDto.CreatedOn,
                NumberOfGames = userDto.NumberOfGames,
                Coefficient = userDto.Coefficient,
                AllScores = userDto.AllScores,
                IsBanned = userDto.IsBanned
            };

            return user;
        }

        public static ICollection<User> GetEntities(this ICollection<UserDto> userDtos)
        {
            return userDtos.Select(GetEntity).ToList();
        }
    }
}
