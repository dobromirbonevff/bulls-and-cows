﻿using Bulls_And_Cows.Data.Entities;
using Bulls_And_Cows.Services.DtoEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bulls_And_Cows.Services.DtoMappers
{
    public static class GameExtension
    {
        public static GameDto GetDto(this Game game)
        {
            var gameDto = new GameDto
            {
                Id = game.Id,
                Score = game.Score,
                UserId = game.UserId
            };

            return gameDto;
        }

        public static ICollection<GameDto> GetDtos(this ICollection<Game> games)
        {
            return games.Select(GetDto).ToList();
        }

        public static Game GetEntity(this GameDto gameDto)
        {
            var game = new Game
            {
                Id = gameDto.Id,
                Score = gameDto.Score,
                UserId = gameDto.UserId
            };

            return game;
        }

        public static ICollection<Game> GetEntities(this ICollection<GameDto> gameDtos)
        {
            return gameDtos.Select(GetEntity).ToList();
        }
    }
}
