﻿using Bulls_And_Cows.Services.DtoEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Contracts
{
    public interface IGameService
    {
        Task<GameDto> CreateGameAsync(Guid userId);
        Task<GameDto> PlayAsync(GameDto gameDto);
    }
}
