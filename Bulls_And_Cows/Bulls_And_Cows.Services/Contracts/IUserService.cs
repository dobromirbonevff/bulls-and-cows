﻿using Bulls_And_Cows.Services.DtoEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bulls_And_Cows.Services.Contracts
{
    public interface IUserService
    {
        public Task<ICollection<UserDto>> GetTop25UsersAsync();
        public Task<ICollection<UserDto>> GetAllUsersAsync(string param);
        public Task<bool> BanUserAsync(Guid id, string description);
        public Task<bool> UnbanUserAsync(Guid id);
    }
}
