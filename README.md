# Bulls And Cows

- Bulls And Cows is a web application, which let its registered users play the game Bulls and Cows


- The public part of the application is available without authentication. It allows site visitors to read the rules of the game and see the top 25 players.


- The administration part of the application is accessible only for admin users. Admin user is able to ban and unban users. 

